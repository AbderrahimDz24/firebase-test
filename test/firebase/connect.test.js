const firebase = require("firebase-admin");
// const firebase = require("firebase");
const serviceAccount = require("./firebase-key.json");

let config = {
    real: {
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://test-dz24.firebaseio.com"
    },
    mock: {
        credential: {
            getAccessToken: () => ({
                expires_in: 0,
                access_token: '',
            }),
        },
        databaseURL: 'ws://localhost:5000'
    }
};
let configChoice = 'mock';

test('Firebase connection', done => {
    firebase.initializeApp(config[configChoice]);
    const db = firebase.database();
    const ref = db.ref('testRef');
    const usersRef = ref.child('users');
    usersRef.set({
        abderrahim: {
            date_of_birth: '17/08/1990',
            full_name: 'Abderrahim HELLACI'
        }
    }, err => {
        if (err) {
            return done(err);
        }
        ref.once('value', function (snapshot) {
            expect(snapshot.val().users.abderrahim.date_of_birth).toBe('17/08/1990');
            expect(snapshot.val().users.abderrahim.full_name).toBe('Abderrahim HELLACI');
            done();
        });
    })
});